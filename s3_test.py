import boto3
from botocore.exceptions import ClientError
import os

s3_endpoint_url = "https://obs.ru-moscow-1.hc.sbercloud.ru"
key_id = "9RFYNEAQ9FG1NX0G6NTS"
access_key = "JqYqqkCfpOiamsZ6Uva75WC6NpmfOzgDXzETeWpA"
bucket = 'hackathon-ecs-36'

def upload(client, filename, object_name=None):
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = os.path.basename(filename)

    try:
        response = client.upload_file(filename, bucket, object_name)
    except ClientError as e:
        print(e)
        return False
    return True


client = boto3.client ( 
            service_name="s3", 
            endpoint_url=s3_endpoint_url, 
            aws_access_key_id=key_id, 
            aws_secret_access_key=access_key, 
            use_ssl=False, 
            verify=False, 
            )

print(upload(client, 'hackathon_part_1.mp4'))

client.download_file(bucket, 'hackathon_part_1.mp4', '2.mp4')

# response = client.list_buckets()

# # Output the bucket names
# print('Existing buckets:')
# for bucket in response['Buckets']:
#     print(f'  {bucket["Name"]}')