import boto3
from botocore.exceptions import ClientError
import os
import redis
import time
import asyncio 
from sound import Filterer as AudioFilterer
from video import Filterer as VideoFilterer
import urllib.request

s3_endpoint_url = "https://obs.ru-moscow-1.hc.sbercloud.ru"
key_id = "9RFYNEAQ9FG1NX0G6NTS"
access_key = "JqYqqkCfpOiamsZ6Uva75WC6NpmfOzgDXzETeWpA"
bucket = 'hackathon-ecs-36'
storage_folder = '/appdata'

ffmpeg_location = './ffmpeg-git-20211029-amd64-static/ffmpeg'

def upload(client, filename, object_name=None):
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = os.path.basename(filename)

    try:
        response = client.upload_file(filename, bucket, object_name)
    except ClientError as e:
        print(e)
        return False
    return True

def download(remote_filename, save_filename):
    urllib.request.urlretrieve(remote_filename, save_filename)


def extract_audio(video_filename, store_folder):
    os.system(f'{ffmpeg_location} -i {video_filename} -vn -acodec pcm_s16le -ac 1 -ar 16000 {store_folder}/audio_clean.wav')

def add_audio(video_filename, audio_filename, store_folder, prefix):
    os.system(f'{ffmpeg_location} -i {video_filename} -i {audio_filename} -c:v copy -map 0:v:0 -map 1:a:0 {store_folder}/{prefix}_result.mp4')

class Processor:
    def __init__(self):
        self.client = boto3.client ( 
            service_name="s3", 
            endpoint_url=s3_endpoint_url, 
            aws_access_key_id=key_id, 
            aws_secret_access_key=access_key, 
            use_ssl=False, 
            verify=False, 
            )
        self.r = redis.Redis(host='redis', port=6379, db=0)
        self.audio_filterer = AudioFilterer()
        self.video_filterer = VideoFilterer()
        print("all models loaded")
    
    def wait(self, store_folder, prefix):
        def exist(f):
            return os.path.exists(os.path.join(store_folder, f))
        while True:
            if exist(prefix + '_audio.json') and exist(prefix + '_video.json'):
                break
            else:
                time.sleep(0.1)

    def upload(self, prefix, store_folder):
        for x in ['result.mp4', 'audio.json', 'video.json']:
            upload(self.client, os.path.join(store_folder, f'{prefix}_{x}'))

    async def process(self, source, prefix):
        store_folder = os.path.join(storage_folder, prefix)
        os.makedirs(store_folder, exist_ok=True)
        video_path = os.path.join(store_folder, 'video_clean.mp4')
        download(source, video_path)
        extract_audio(video_path, store_folder)
        self.r.rpush('video_queue', store_folder)
        self.r.rpush('audio_queue', store_folder)
        input_coroutines = [
            self.audio_filterer.get_filtered_waveform(os.path.join(store_folder, 'audio_clean.wav'), prefix),
            self.video_filterer.get_filtered_video(video_path, prefix)
        ]
        await asyncio.gather(*input_coroutines, return_exceptions=True)
        # self.wait(storage_folder, prefix)
        add_audio(os.path.join(store_folder, 'video.mp4'), os.path.join(store_folder, 'audio.wav'), store_folder, prefix)
        self.upload(prefix, store_folder)
        return True
