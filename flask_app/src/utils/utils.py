import torch


def get_device() -> str:
    """Returns available torch device

    Returns:
        str: available torch device
    """

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    return device