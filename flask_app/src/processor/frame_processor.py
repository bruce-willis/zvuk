from typing import List
from pathlib import Path
from PIL import Image
import pickle
from collections import Counter

import numpy as np
import pandas as pd

import torch
import faiss
from facenet_pytorch import MTCNN

from src.models import InceptionResnetV1
from src.utils import PathOrStr, get_device


class FrameProcessor:

    N_FEATURES = 512  # recognizer features dimensionality
    K = 3  # number of closest embeddings to search in index
    CLASSES = 1251
    CLASS_TO_IDX = './data/class_to_idx.pkl'
    DIST_THRESH = 0.3

    def __init__(self,
                 embeddings_dir_path: PathOrStr = './data/embeddings',
                 embeddings_df_path: PathOrStr = './data/embeddings.csv',
                 recognizer_path: PathOrStr = './models/InceptionResnetV1_finetuned_voxceleb1.pt'):

        self._embeddings_dir = Path(embeddings_dir_path)
        self._embeddings_df = pd.read_csv(embeddings_df_path)
        self._recognizer_path = Path(recognizer_path)
        self._device = get_device()

        self._detector, self._recognizer = self._load_models()
        self._index = self._build_index()
        self._blacklist = self._get_blacklist()

    @property
    def blacklist(self) -> List:
        return self._blacklist

    def process(self, frame: np.ndarray):
        frame_output = frame.copy()
        frame_converted = Image.fromarray(frame)

        # detect faces
        boxes, _ = self._detector.detect(frame_converted)

        # no faces found
        if len(boxes) == 0:
            return frame_output, {}

        # crop faces
        frame_cropped = self._detector.extract(frame_converted, boxes, None)
        frame_cropped = frame_cropped.to(self._device)

        if len(frame_cropped.shape) != 4:
            frame_cropped = frame_cropped.unsqueeze(0)

        # compute embeddings, find identity
        embedding, pred = self._recognizer(frame_cropped)
        dists, idxs = self._index.search(embedding.detach().cpu().numpy(), self.K)

        frame_blurred = frame.copy()
        results = []
        for i, idx in enumerate(idxs):
            idx = idx[idx < len(self._embeddings_df)]

            dist = dists[i][0]
            if dist >= self.DIST_THRESH:
                continue

            celebs = self._embeddings_df.iloc[idx]['celebrity'].values
            most_common_celeb = Counter(celebs).most_common(1)[0][0]

            if most_common_celeb not in self.blacklist:
                continue

            x1, y1, x2, y2 = boxes[i]
            x1 = int(x1)
            y1 = int(y1)
            x2 = int(x2)
            y2 = int(y2)

            frame_blurred[y1: y2, x1: x2] = 0
            results.append({'corner_1': [x1, y1], 'corner_2': [x2, y2]})

        return frame_blurred, results

    def _load_models(self):
        detector = MTCNN(keep_all=True, device=self._device).eval()
        recognizer = InceptionResnetV1(num_classes=self.CLASSES, classify=True, device=self._device).eval()
        ckpt = torch.load(self._recognizer_path, map_location='cpu')
        recognizer.load_state_dict(ckpt)
        return detector, recognizer

    def _build_index(self):
        index = faiss.IndexFlatL2(self.N_FEATURES)

        embeddings = []
        for _, row in self._embeddings_df.iterrows():
            idx = row['idx']
            path = self._embeddings_dir / f'{idx:07}.pt'
            embedding = torch.load(path)
            embeddings.append(embedding)

        embeddings = torch.cat(embeddings, dim=0)
        index.add(embeddings.detach().cpu().numpy())
        return index

    def _get_blacklist(self):
        with open(self.CLASS_TO_IDX, 'rb') as f:
            blacklist = pickle.load(f)
        return blacklist


if __name__ == '__main__':

    import cv2
    processor = FrameProcessor()
    print('1')

    im_path = '/home/kinakh/Datasets/zippedFaces/unzippedFaces/Rob_Reiner/1.6/d8H0ZdING7Y/0001725.jpg'
    img = cv2.imread(im_path)
    img = cv2.resize(img, (512, 512))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    processor.process(img)
    # print(processor.blacklist)
