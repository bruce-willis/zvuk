from pathlib import Path
import json

import cv2

from src.processor.frame_processor import FrameProcessor
from src.utils import PathOrStr


class VideoProcessor:

    def __init__(self,
                 embeddings_dir_path: PathOrStr = './data/embeddings',
                 embeddings_df_path: PathOrStr = './data/embeddings.csv',
                 recognizer_path: PathOrStr = './models/InceptionResnetV1_finetuned_voxceleb1.pt'):

        self._frame_processor = FrameProcessor(embeddings_dir_path, embeddings_df_path, recognizer_path)

    def process(self, work_dir: PathOrStr):
        work_dir = Path(work_dir)

        for vid_path in work_dir.glob('*.mp4'):
            self.process_video(vid_path)

    def process_video(self, video_path: PathOrStr, prefix=None):
        parent_dir = str(Path(video_path).parents[0])
        out_video_path = Path(video_path).parent / f'video.mp4'
        out_json_path = Path(video_path).parent / f'{prefix}_video.json'
        result = []

        video = cv2.VideoCapture(str(video_path))
        fps = video.get(cv2.CAP_PROP_FPS)
        width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

        frame_time = 1. / fps

        writer = cv2.VideoWriter(str(out_video_path),
                                 cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
                                 fps, (width, height))

        frame_idx = 0
        while video.isOpened():
            success, frame = video.read()

            if not success or frame is None:
                break

            timestamp = frame_idx / fps
            frame_blurred, frame_result = self._frame_processor.process(frame)

            for res in frame_result:
                res['time_start'] = timestamp
                res['time_end'] = timestamp + frame_time
                result.append(res)

            frame_idx += 1
            writer.write(frame_blurred)

        result = {'result': result}
        with open(out_json_path, 'w') as f:
            json.dump(result, f)
