import shutil
from pathlib import Path
import os
from src.processor.video_processor import VideoProcessor

class Filterer:
    def __init__(self):
        self.processor = VideoProcessor()

    async def get_filtered_video(self, path, prefix):
        self.processor.process_video(path, prefix)
