import wave
from vosk import Model, KaldiRecognizer
import json
import editdistance
from scipy.io.wavfile import read, write
from pathlib import Path


def compute_cer(preds, labels):
    """
    Arguments:
        results (list): list of ground truth and
        predicted sequence pairs.
    Returns the CER for the full set.
    """
    distance = [
        editdistance.eval(label, pred) / len(label)
        for label, pred in zip(labels, preds)
        if len(label) > 0
    ]
    return min(distance)


class Filterer:
    def __init__(self):
        self.model = Model("model")
        with open("names.txt") as f:
            self.words = f.read().split(' ')
        self.censor = read('censor2.wav')[1]

    def init(self):
        self.rec = KaldiRecognizer(self.model, 16000)
        self.rec.SetWords(True)

    async def get_filtered_waveform(self, path, prefix):
        out_path = path.replace("_clean", "")
        out_path_json = str(Path(path).parents[0]) + '/' + f"{prefix}_audio.json"

        self.init()
        file_to_corrupt = read(path)[1]
        output = []
        wf = wave.open(path, "rb")
        while True:
            data = wf.readframes(4000)
            if len(data) == 0:
                break
            if self.rec.AcceptWaveform(data):
                pass  # print(self.rec.Result())
        results = json.loads(self.rec.FinalResult())
        for result in results['result']:
            if result['conf'] > 0.2:
                cer_res = compute_cer([result['word']] * len(self.words), self.words)
                if cer_res < 0.1:
                    output.append({"time_start": result['start'], "time_end": result['end']})
                    file_to_corrupt[
                        int(result['start'] * 16000) : int(result['end'] * 16000)
                    ] = self.censor[: int(result['end'] * 16000) - int(result['start'] * 16000)]
        write(out_path, 16000, file_to_corrupt)
        with open(out_path_json, "w") as f:
            output = {'result': output}
            json.dump(output, f)
        return


if __name__ == "__main__":
    cl = Filterer()
    cl.get_filtered_waveform('audio_clean.wav')


