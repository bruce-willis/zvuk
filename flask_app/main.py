from PIL import Image

import cv2

import torch
from facenet_pytorch import MTCNN, InceptionResnetV1

from src.processor.frame_processor import FrameProcessor
from src.utils import get_device


# hyper parameters
device = get_device()

# define models
# detector = MTCNN(keep_all=True, device=device).eval()
# recognizer = InceptionResnetV1(num_classes=1251, classify=True, device=device).eval()
# recognizer_path = './notebooks/InceptionResnetV1_finetuned_voxceleb1.pt'
# ckpt = torch.load(recognizer_path)
# recognizer.load_state_dict(ckpt)


def run():
    p = './data/hackathon_part_1.mp4'

    video = cv2.VideoCapture(p)

    success, img = video.read()

    processor = FrameProcessor()

    while success:
        success, img = video.read()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        # img_converted = Image.fromarray(img)
        # img_with_boxes = img.copy()
        #
        # detect faces
        # boxes, _ = detector.detect(img_converted)
        #
        # crop faces
        # img_cropped = detector.extract(img_converted, boxes, None)
        # img_cropped = img_cropped.to(device)
        #
        # if len(img_cropped.shape) != 4:
        #     img_cropped = img_cropped.unsqueeze(0)
        # embedding = recognizer(img_cropped)

        # print(embedding.shape)
        #
        # draw daces
        # for box in boxes:
        #     x1, y1, x2, y2 = box
        #     x1 = int(x1)
        #     y1 = int(y1)
        #     x2 = int(x2)
        #     y2 = int(y2)

            # img_with_boxes = cv2.rectangle(img_with_boxes, (x1, y1), (x2, y2), (0, 0, 255), -1)

        img_with_boxes, res = processor.process(img)

        cv2.namedWindow('faces', cv2.WINDOW_FREERATIO)
        cv2.imshow('faces', img_with_boxes)
        cv2.waitKey(0)

        if not success or img is None:
            print('Finish')
            break


if __name__ == '__main__':
    run()
