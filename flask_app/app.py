import asyncio
from flask import Flask
from flask import request, jsonify
from random import sample
import traceback
from endpoint import Processor
import asyncio

processor = Processor()

server = Flask(__name__)

def run_request():
    source = request.json['source'] # request.form.get('source')
    prefix = request.json['prefix'] # request.form.get('prefix')
    message = 'ok'
    code = 200
    try:
        asyncio.get_event_loop().run_until_complete(processor.process(source, prefix))
    except Exception:
        message = traceback.format_exc()
        code = 501
    finally:
        return jsonify(message=message, code=code)

@server.route('/recognize', methods=['GET', 'POST'])
def hello_world():
    if request.method == 'GET':
        return 'The model is up and running. Send a POST request'
    else:
        return run_request()
